@APIPetStore
Feature: Probar Api DELETE de Pet Store

  Background:
    * url 'https://petstore.swagger.io/v2/'

  # 4. Eliminar mascota
  # aqui no utilizamos niguna variable externa, creamos la mascota y la eliminamos
  @id:3 @EliminarMascota
  Scenario: T-API-CA4- Eliminar Mascota
    * def datos = call read("crearMascota.feature@CrearMascota")
    Given path 'pet/'+ datos.idMascota
    When method DELETE
    Then status 200
    And match response.message == "1"