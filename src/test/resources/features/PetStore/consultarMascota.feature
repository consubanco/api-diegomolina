@APIPetStore
Feature: Probar Api GET de Pet Store

  Background:
    * url 'https://petstore.swagger.io/v2/'

  # 2. Verificar que la mascota fue creada correctamente
  # se utiliza un archivo csv para realizar el match del nombre
  @id:2 @ConsultarMascota
  Scenario Outline: T-API-CA2- Consultar Mascota
    * def datos = call read("crearMascota.feature@CrearMascota")
    Given path 'pet/' + datos.idMascota
    When method GET
    Then status 200
    And match response.name == '<nombreMascota>'
Examples:
  | read('classpath:/data/input/datoMascota.csv') |
