@APIPetStore
Feature: Probar Api PUT de Pet Store

  Background:
    * url 'https://petstore.swagger.io/v2/'

  # 3. Modificar el nombre de mascota por PugCarlino_Luna
  # actualizamos el nombre desde una variable externa
  @id:3 @ActualizarMascota
  Scenario Outline: T-API-CA3- Actualizar Mascota
    * def datos = call read("crearMascota.feature@CrearMascota")
    * set datos.mascotaBody.name = '<nameDog>'
    * print datos.mascotaBody
    Given path 'pet'
    And request datos.mascotaBody
    When method PUT
    Then status 200
    And match response.name == '<nameDog>'
Examples:
      |nameDog|
      |PugCarlino_Luna|