@APIPetStore
Feature: Probar Api POST de Pet Store

  Background:
    * url 'https://petstore.swagger.io/v2/'

  # 1. Adiciona una mascota nueva donde el nombre tenga el siguiente formato Pug_Luna
  # utilizando un json para cargar en el Body pasando parametros al Json
  @id:1 @CrearMascota
  Scenario Outline: T-API-CA1- Crea Mascota
    * def mascotaBody = read('classpath:../data/input/newMascota.json')
    Given path 'pet'
    And request mascotaBody
    When method POST
    Then status 200
    And match response.name == "Pug_Luna"
    * def idMascota = response.id
    * def nombre = response.name
    Examples:
      | idCreate | nameDog  |
      | 1        | Pug_Luna |