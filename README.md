# Versiones
Java: 17
Gradle: 8.7
Karate: 1.13
IDE: intellij idea community edition 2024.1

# Para ejecutar los test API desde IDE
clic derecho en archivo src/test/com/template/runners/ApiRunner y ejecutar la prueba

# Reporte Karate
el reporte se encuentra en la siguiente ruta
"build/karate-reports/karate-summary.html"